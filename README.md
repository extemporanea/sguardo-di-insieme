# Sguardo d'insieme

Qui cerchiamo di raccogliere in maniera *schematica* lo stato dei vari progetti e attività di eXtemporanea.

Questo diagramma è scritto con mermaid https://mermaid-js.github.io/mermaid/

```mermaid
graph LR
  %% Questo è un diagramma left-to-right (LR)

  %% Ogni nodo di base (root) è un progetto e per esser specificato necesssita di un ID e di un label.

  %% La sintassi per ottenere un nodo con angoli rotondi è id(label)
  %% La sintassi per ottenere un nodo con angoli retti è id[label]

  %% I nodi si collegano con frecce. Queste si scrivono così -->

  %% La fina di una linea di testo di un label ha un marcatore <br>



  website(Sito Web):::root --> maintain[Mantenimento]

  sg(Scienceground 2020):::root -->archive[Archivio]

  biblioSCNR(Biblio Scienceground-CNR):::root --> prepare[Preparazione <br>libreria su Zotero]--> buy[Acquisto libri] --> path[Creazione percorso principale]

  biblioX(Biblio eXtemporanea):::root --> setup[Proposta]



  %% Stile per le caselle di base (root). Per specificare che una casella è una base si aggiunge :::root dopo l'id e il testo
  classDef root fill:#f96;
```
